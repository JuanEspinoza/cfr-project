import React from 'react'
import "../Photo.css";
import {Link, Navigate }from 'react-router-dom';
import { useLocalStorage } from "../helpers/user.local.storage";
import { logout } from "../actions/auth";
import AuthService from "../services/auth.service";


export const Menu = () => {
    const [user, setUser] = useLocalStorage("user", null);
    function logout() {
        AuthService.logout();
        //navigate("/");
        <Navigate to={"/"} replace={true}/>
        window.location.reload();
        console.log("Goingt to navigate to main route");
    }
    let items;
    if(user!=null){
        items =
                <ul className="navbar-nav ml-auto">
                    <li className="nav-item">
                        <Link to={"/profile"} className="nav-link">
                            Profile                    
                        </Link>
                    </li>
                    <li className="nav-item">
                        <a className='nav-link' onClick={logout}>
                            Logout
                        </a>                    
                    </li>
                </ul>        
    }else{
        items = 

        <ul className="navbar-nav ml-auto">
            <li className="nav-item">
                <Link to={"/login"} className="nav-link">
                    Sign In
                </Link>
            </li>
            <li className="nav-item">
                <Link to={"/register"} className="nav-link">
                    Sign Up                    
                </Link>
            </li>
        </ul>        
    }

    return (
        <div>
            <nav className="navbar fixed-top navbar navbar-expand-sm navbar-dark bg-dark">
                <Link to={"/"}  className="navbar-brand">
                    Welcome ;) 
                </Link>
                {items}
            </nav>
        </div>
      )
    
}