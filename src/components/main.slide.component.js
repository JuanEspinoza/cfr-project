import React, { Fragment } from 'react'
import AOS from 'aos';
import 'aos/dist/aos.css'; 
import "../Photo.css";
export default function MainSlide() {
    AOS.init();
    return (
        <Fragment>
            <div>
                <img className="img-fluid main-photo" src="https://images.pexels.com/photos/5483077/pexels-photo-5483077.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1" alt="Responsive image" />
            </div>
            <div className="section text-center" data-aos="flip-up">
                <h1>All our creators are here :D</h1>
            </div>

            <div className='container section'>
            <div className="row card-deck">
            <div className=" col-md-4 col-sm-4 card" data-aos="fade-down">
                    <img className="card-img-top rounded-circle" 
                    src="https://images.unsplash.com/photo-1570295999919-56ceb5ecca61?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8dXNlciUyMHByb2ZpbGV8ZW58MHx8MHx8&w=1000&q=80" alt="Card image cap"/>
                    <div className="card-body">
                        <h5 className="card-title">Marcos Juarez</h5>
                        <h6 className="card-subtitle mb-2 text-muted">Project Manager</h6>
                        <p className="card-text">I like to drink coffe and share time with family </p>
                    </div>
                </div>
                <div className=" col-md-4 col-sm-4 card" data-aos="fade-down">
                    <img className="card-img-top rounded-circle" 
                    src="https://images.unsplash.com/photo-1570295999919-56ceb5ecca61?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8dXNlciUyMHByb2ZpbGV8ZW58MHx8MHx8&w=1000&q=80" alt="Card image cap"/>
                    <div className="card-body">
                        <h5 className="card-title">Marcos Juarez</h5>
                        <h6 className="card-subtitle mb-2 text-muted">Project Manager</h6>
                        <p className="card-text">I like to drink coffe and share time with family </p>
                    </div>
                </div>
            </div>
            </div>

            <footer className="footer page-footer font-small bg-dark">
                <div className="container">
                    <div className="row">
                        <div className='col-md-4 col-xs-12'></div>
                        <div className="col-md-4 col-xs-12">
                            <form>
                                <input type="email" class="form-control" placeholder="Enter email"/>                                
                            </form>
                        </div>
                        <div className='col-md-4 col-xs-12'></div>  
                    </div>
                    <div className='row'>
                        <div className='col-md-4 col-xs-12'></div>
                        <div className='col-md-4 col-xs-12'>
                            <p className='footer-copyright text-center text-light'>
                                © 2023 Copyright:All rights reserved for Codigo Facilito React Project
                            </p>
                        </div>
                        <div className='col-md-4 col-xs-12'></div>
                    </div>
                </div>
            </footer>
        </Fragment>
    )
}
