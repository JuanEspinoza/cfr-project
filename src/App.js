import React, { Component } from "react";
import { connect } from "react-redux";
//import { Router, Switch, Route, Link } from "react-router-dom";
import { BrowserRouter,Routes ,Route} from 'react-router-dom';

import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";
import { logout } from "./actions/auth";
import { clearMessage } from "./actions/message";
//import { history } from './helpers/history';
import EventBus from "./common/EventBus";
import Routering from "./routes/router.grl"
import {Menu}  from "./components/menu.component";

import Login from "./components/login.component";
import Register from "./components/register.component";
import Profile from "./components/profile.component";
import User from "./components/user.component";
import MainSlide from "./components/main.slide.component";
import { ProtectedRoute } from "./routes/protected.route";
class App extends Component {
  constructor(props) {
    super(props);
    this.logOut = this.logOut.bind(this);

    this.state = {
      currentUser: undefined,
    };

   /* history.listen((location) => {
      props.dispatch(clearMessage()); 
    });*/
  }

  componentDidMount() {
    console.log(this.props.user);
    const user = this.props.user;

    if (user) {
      this.setState({
        currentUser: user
      });
    }

    EventBus.on("logout", () => {
      this.logOut();
    });
  }

  componentWillUnmount() {
    EventBus.remove("logout");
  }

  logOut() {
    this.props.dispatch(logout());
    this.setState({
      currentUser: undefined,
    });
  }

  render() {
    const { currentUser} = this.state;

    return (
      <BrowserRouter>
        <Menu/>
        <Routes>
            <Route path="/" element={<MainSlide/>}></Route>
            <Route path="/login" element={<Login/>}></Route>
            <Route path="/register" element={<Register/>}></Route>
            <Route path="/profile" 
                      element={
                        <ProtectedRoute>
                          <Profile/>
                        </ProtectedRoute>
                      }></Route>
            <Route path="/user" element={<User/>}></Route>
          </Routes>
      </BrowserRouter>
    );
  }
}

function mapStateToProps(state) {
  const { user } = state.auth;
  return {
    user,
  };
}

export default connect(mapStateToProps)(App);