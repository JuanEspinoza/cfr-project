import { Navigate } from "react-router-dom";
//import { useAuth } from "../common/auth-context";
import { useLocalStorage } from "../helpers/user.local.storage";

export const ProtectedRoute = ({ children }) => {
    const [user, setUser] = useLocalStorage("user", null);
    console.log("Entando al metodo de protected Route");
    //const { user } = useAuth();
    console.log("Despues de obtener los datos de la informacion");
    if (!user) {
        // user is not authenticated
        return <Navigate to="/" />;
    }
    return children;
};