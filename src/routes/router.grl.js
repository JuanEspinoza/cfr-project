/**
    GENERAL FILE FOR ROUTE CONFIGURATION
*/
import Login from "../components/login.component";
import Register from "../components/register.component";
import Profile from "../components/profile.component";
import User from "../components/user.component";
import MainSlide from "../components/main.slide.component";
import { useRoutes } from "react-router-dom";
import ProtectedRoutes from "../routes/protected.route"

export default function Routering (){
    
    const routes = useRoutes([
        {
            path:"/",
            element:<MainSlide/>
        },{
            path:"/login",
            element:<Login/>
        },{
            path:"/register",
            element:<Register/>
        },{
            path:"/profile",
            element:<Profile/>
        },{
            path:"/user",
            element:<User/>
        }
    ]);
    return routes;
}